package com.jiomitwa.kafka.streams.topology;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaSimpleProducer {

	public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        new KafkaSimpleProducer();
    }
	
	public KafkaSimpleProducer()
	{
		 Properties props = new Properties();
		 props.put("bootstrap.servers", "scspa0316505001.rtp.openenglab.netapp.com:9092,scspa0316505002.rtp.openenglab.netapp.com:9092,scspa0316505003.rtp.openenglab.netapp.com:9092");
		 props.put("acks", "all");
		 props.put("retries", 0);
		 props.put("batch.size", 16384);
		 props.put("linger.ms", 1);
		 props.put("buffer.memory", 33554432);
		 props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		 props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		 Producer<String, String> producer = new KafkaProducer<>(props);
		 for (int i = 0; i < 100; i++)
		 {
		     String s = new Date().toString();
			 producer.send(new ProducerRecord<String, String>("src-topic", Integer.toString(i), "Tunu How do you do?"));
		 }
		 System.out.println("produced 100 items");
		 producer.close();
	}
}
