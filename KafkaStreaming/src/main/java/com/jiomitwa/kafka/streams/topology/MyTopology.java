package com.jiomitwa.kafka.streams.topology;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.StateStoreSupplier;
import org.apache.kafka.streams.processor.TopologyBuilder;
import org.apache.kafka.streams.state.Stores;

public class MyTopology {

	public static void main(String[] args)
	{
		Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-linesplit");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "scspa0316505001.rtp.openenglab.netapp.com:9092,scspa0316505002.rtp.openenglab.netapp.com:9092,scspa0316505003.rtp.openenglab.netapp.com:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        
        StateStoreSupplier countStore = Stores.create("Counts1")
        		.withKeys(Serdes.String())
        		.withValues(Serdes.Long())
        		.persistent()
        		.build();
        
		TopologyBuilder builder = new TopologyBuilder();
		builder.addSource("SOURCE", "src-topic")
		.addProcessor("PROCESS1", () -> new MyProcessor(), "SOURCE")
		.addStateStore(countStore, "PROCESS1")
		.addProcessor("PROCESS2", () -> new MyProcessor2(), "SOURCE")
		.addSink("SINK1", "sink-topic1", "PROCESS1")
		.addSink("SINK2", "sink-topic2", "PROCESS2");
		
	      final KafkaStreams streams = new KafkaStreams(builder, props);
	        final CountDownLatch latch = new CountDownLatch(1);
	        // attach shutdown handler to catch control-c
	        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
	            @Override
	            public void run() {
	                streams.close();
	                latch.countDown();
	            }
	        });
	 
	        try {
	            streams.start();
	            latch.await();
	        } catch (Throwable e) {
	            System.exit(1);
	        }
	        System.exit(0);
	}
}
