package com.jiomitwa.kafka.streams.topology;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopologyBuilder;

public class MyTopology2 {

	public static void main(String[] args)
	{
		Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-linesplit2");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "scspa0316505001.rtp.openenglab.netapp.com:9092,scspa0316505002.rtp.openenglab.netapp.com:9092,scspa0316505003.rtp.openenglab.netapp.com:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        //props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, "exactly_once");
     // Records should be flushed every 10 seconds. This is less than the default
        // in order to keep this example interactive.
        //props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);//default is 30Sec
      //  props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 6);
       // props.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, 10);
       // props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 1000); //default 1000 
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100); //default 500
        
        //each batch get processed and then get committed 
        //max.poll.records
    
		TopologyBuilder builder = new TopologyBuilder();
		builder.addSource("SOURCE", "src-topic")
		.addProcessor("PROCESS2", () -> new MyProcessor2(), "SOURCE")
		.addSink("SINK2", "sink-topic2", "PROCESS2");
		
	      final KafkaStreams streams = new KafkaStreams(builder, props);
	     
	        final CountDownLatch latch = new CountDownLatch(1);
	        // attach shutdown handler to catch control-c
	        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
	            @Override
	            public void run() {
	                streams.close();
	                latch.countDown();
	            }
	        });
	 
	        try {
	            streams.start();
	            latch.await();
	        } catch (Throwable e) {
	            System.exit(1);
	        }
	        System.exit(0);
	}
}
