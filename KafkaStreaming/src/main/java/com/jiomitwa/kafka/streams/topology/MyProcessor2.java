package com.jiomitwa.kafka.streams.topology;

import java.util.Date;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

public class MyProcessor2 implements Processor<String, String> {
	private ProcessorContext context;
	private KeyValueStore<String, Long> kvStore;

	@Override
	@SuppressWarnings("unchecked")
	public void init(ProcessorContext context) {
		// keep the processor context locally because we need it in punctuate()
		// and commit()
		this.context = context;

		// call this processor's punctuate() method every 1000 milliseconds.
		//this.context.schedule(1000);

		// retrieve the key-value store named "Counts"
		//this.kvStore = (KeyValueStore<String, Long>) context.getStateStore("Counts1");
	}

	@Override
	public void process(String dummy, String line) {
		
		System.out.println(new Date().toString()+" " + Thread.currentThread().getId() + " Processor2 TaskId "+ context.taskId()+" Partition " + context.partition()+" "+context.offset()+" "+ line);
		//String[] words = line.toLowerCase().split(" ");
		
		
		//if(line.endsWith("25")) System.exit(0);
		context.forward(line, line+"_COMPLETD");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		//context.commit();
		/*for (String word : words) {
			Long oldValue = this.kvStore.get(word);

			if (oldValue == null) {
				this.kvStore.put(word, 1L);
			} else {
				this.kvStore.put(word, oldValue + 1L);
			}
		}*/
	}

	@Override
	public void punctuate(long timestamp) {
		System.out.println("Processor 2 : -> Punctuate is getting called");
		KeyValueIterator<String, Long> iter = this.kvStore.all();
	
		while (iter.hasNext()) {
			KeyValue<String, Long> entry = iter.next();
			context.forward(entry.key, entry.value.toString());
		}

		iter.close();
		// commit the current processing progress
		context.commit();
	}

	@Override
	public void close() {
		// close any resources managed by this processor.
		// Note: Do not close any StateStores as these are managed
		// by the library
	}
};
