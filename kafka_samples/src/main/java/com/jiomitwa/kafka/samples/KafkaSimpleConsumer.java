package com.jiomitwa.kafka.samples;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class KafkaSimpleConsumer {

	public KafkaSimpleConsumer()
	{
		 Properties props = new Properties();
		 props.put("bootstrap.servers", "scspa0316505001.rtp.openenglab.netapp.com:9092,scspa0316505002.rtp.openenglab.netapp.com:9092,scspa0316505003.rtp.openenglab.netapp.com:9092");
	     props.put("group.id", "test");
	     props.put("enable.auto.commit", "true");
	     props.put("auto.commit.interval.ms", "1000");
	     props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	     props.put("value.deserializer", "org.apache.kafka.common.serialization.LongDeserializer");
	     KafkaConsumer<String, Long> consumer = new KafkaConsumer<>(props);
	     consumer.subscribe(Arrays.asList("streams-wordcount-output"));
	     while (true) {
	         ConsumerRecords<String, Long> records = consumer.poll(100);
	         for (ConsumerRecord<String, Long> record : records)
	             System.out.println(record.offset()+" "+record.key()+" "+record.value());
	         //System.out.println("batch printed");
	     }	
	}
}
